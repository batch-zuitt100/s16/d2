let numberA = null
let numberB = null
let operation = null

//to retrieve an element from the webpage, we can use querySelector
let inputDisplay = document.querySelector('#txt-input-display')
//The document refers to the whole webpage and querySelector is used to select a specific object(HTML object)
//The query selector funtion takes a string input is formatted like a CSS selector
//This allows us to get specific element such as CSS selectors


/*document.getElementById()
document.getElementByClass()
document.getElementsByTagName()*/

//btnAdd
//btnSubtract
//btnMultiply
//btnDivide
//btnEqual
//btnDecimal
//btnClearAll
//btnBackspace

let btnNumbers = document.querySelectorAll('.btn-numbers');

let btnAdd = document.querySelector('#btn-add');
let btnSubtract = document.querySelector('#btn-subtract');
let btnMultiply = document.querySelector('#btn-multiply');
let btnDivide = document.querySelector('#btn-divide');
let btnEqual = document.querySelector('#btn-equal');
let btnDecimal = document.querySelector('#btn-decimal');

let btnClearAll = document.querySelector('#btn-clear-all');
let btnBackspace = document.querySelector('#btn-backspace');

btnNumbers.forEach(function(btnNumber){
	btnNumber.onclick = () => {
		inputDisplay.value += btnNumber.textContent;
	}
})

btnAdd.onclick = () => {
	if (numberA == null) {
		numberA = Number(inputDisplay.value);
		operation = 'addition';
		inputDisplay.value = null;
	} 
	else if (numberB == null) {
		numberB = Number(inputDisplay.value);
		numberA = numberA + numberB;
		operation = 'addition';
		numberB.value = null;
		inputDisplay.value = null;
	}
}


btnSubtract.onclick = () => {
	if (numberA == null) {
		numberA = Number(inputDisplay.value);
		operation = 'subtraction';
		inputDisplay.value = null;
	} 
	else if (numberB == null) {
		numberB = Number(inputDisplay.value);
		numberA = numberA - numberB;
		operation = 'subtraction';
		numberB.value = null;
		inputDisplay.value = null;
	}
}

btnMultiply.onclick = () => {
	if (numberA == null) {
		numberA = Number(inputDisplay.value);
		operation = 'multiplication';
		inputDisplay.value = null;
	} 
	else if (numberB == null) {
		numberB = Number(inputDisplay.value);
		numberA = numberA * numberB;
		operation = 'multiplication';
		numberB.value = null;
		inputDisplay.value = null;
	}
}

btnDivide.onclick = () => {
	if (numberA == null) {
		numberA = Number(inputDisplay.value);
		operation = 'division';
		inputDisplay.value = null;
	} 
	else if (numberB == null) {
		numberB = Number(inputDisplay.value);
		numberA = numberA / numberB;
		operation = 'division';
		numberB.value = null;
		inputDisplay.value = null;
	}
}

btnEqual.onclick = () => {
	if (numberB == null && inputDisplay.value !== ''){
		numberB = inputDisplay.value;
	}
	if (operation == 'addition') {
		inputDisplay.value = Number(numberA) + Number(numberB);
	}
	if (operation == 'subtraction') {
		inputDisplay.value = Number(numberA) - Number(numberB);
	}
	if (operation == 'multiplication') {
		inputDisplay.value = Number(numberA) * Number(numberB);
	}
	if (operation == 'division') {
		inputDisplay.value = Number(numberA) / Number(numberB);
	}

}



btnClearAll.onclick = () => {
	numberA = null;
	numberB = null;
	operation = null;
	inputDisplay.value = null;
}

btnBackspace.onclick = () => {
	inputDisplay.value = inputDisplay.value.slice(0, -1)
}

btnDecimal.onclick = () => {
	if (!inputDisplay.value.includes('.')) {
		inputDisplay.value = inputDisplay.value + btnDecimal.textContent;
	}
}


function myFunction() {
          var x = document.getElementById("myInput1").value;
          var y = document.getElementById("myInput2").value;

          document.getElementById("lastName").innerHTML = x + ' ' + y;
        }